# A Simple Arxiv Reader for Academic Purpose


## Motivation of this project

[Arxiv](https://arxiv.org) is the most popular pre-print website for researchers. 

<img src="/app/img/arxiv.png" alt="arxiv homepage" style="zoom:50%;" />

Every day, the first thing after I reach my lab  is to open this site and browse newly published papers. However, the key problem is there are too many new papers published every day, so it's difficult for me to recognize which of them is suitable for me. Every morning I have to pay nearly half hour to example them one by one.